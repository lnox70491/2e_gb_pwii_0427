import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class FuncionariosService {
  private readonly URL_Y = "https://3000-nicollyalme-2egbapi0810-bamsxjpzdbf.ws-us79.gitpod.io/"
  private readonly URL_N = ""
  private readonly URL = this.URL_Y

  constructor(
    private http: HttpClient
  ) { }

  buscarTodosOsFuncionarios(): Observable<any> {
    return this.http.get<any>(`${this.URL}funcionarios`)
  }
}
