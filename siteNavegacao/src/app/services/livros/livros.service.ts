import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LivrosService {
private readonly URL_N = "https://3000-nicollyalme-2egbapi0810-urt250itrwh.ws-us77.gitpod.io/"
private readonly URL = this.URL_N

  constructor(
    private http: HttpClient
  ) { }

  buscarLivros(): Observable<any>{
    return this.http.get<any>(`${this.URL}livros`)
  }
}
