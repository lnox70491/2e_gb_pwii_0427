export class Livro {
    titulo: String = "";
    anoPublicacao: number = 0;
    edicao: number = 0;
    idioma: String = "";
}