import { Pessoa } from './Pessoa';

export class Funcionario extends Pessoa {
    data_admissao: Date = new Date
    salario: number = 0
    cargo: Cargo = 0
}

enum Cargo{
    atendente = 0,
    repositor = 1,
    logistica = 2,
    gestor = 3
}
