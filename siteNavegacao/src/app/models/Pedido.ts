
   export class Pedidos {
    data_pedido: string = ""
    previsao_entrega: string = ""
    observacoes: string = ""
    
}
 enum forma_pagamento{
    "A vista- pix" = 0,
    "A vista- Boleto" = 1,
    "A vista- Debito" = 2,
    "1x- Crédito (5% de desconto)" = 3,
    "3x- Crédito (2% de desconto)" = 4,
    "12x- Crédito (sem juros)" = 5
 }

 enum status{
    "Aguardando pagamento"=0,
    "Pagamento confirmado" = 1,
    "Preparação do envio" = 2,
    "Em transporte" = 3,
    "Pedido entregue" = 4,
    "Devolução" = 5,
    "Em análise" = 6,
    "Cancelado" = 7
 }



