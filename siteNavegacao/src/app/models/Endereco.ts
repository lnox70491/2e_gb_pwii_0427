import { Pessoa } from './Pessoa';

export class Endereco extends Pessoa {
    id_cliente: number = 0
    descricao: string = ""
    complemento: string = ""
}