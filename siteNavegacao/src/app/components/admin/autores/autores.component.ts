import { AutoresService } from 'src/app/services/autores/autores.service';
import { Autor } from './../../../models/Autor';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-autores',
  templateUrl: './autores.component.html',
  styleUrls: ['./autores.component.scss']
})
export class AutoresComponent implements OnInit {
  autores: Autor[]

  constructor(
    private readonly autoresService: AutoresService
  ) {
    this.autores = []
  }

  ngOnInit(): void {
    this.carregarAutores()
  }

  private carregarAutores(): void {
    this.autoresService.buscarTodosOsAutores()
    .subscribe({
      next: (resposta) => {
        this.autores = resposta.results
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }
}
