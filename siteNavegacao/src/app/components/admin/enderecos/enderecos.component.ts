import { Component, OnInit } from '@angular/core';
import { EnderecosService } from 'src/app/services/enderecos/enderecos.service';
import { Endereco } from 'src/app/models/Endereco';

@Component({
  selector: 'app-enderecos',
  templateUrl: './enderecos.component.html',
  styleUrls: ['./enderecos.component.scss']
})
export class EnderecosComponent implements OnInit {
  enderecos: Endereco[]

  constructor(
    private readonly enderecosService: EnderecosService
  ) { 
    this.enderecos = []
  }

  ngOnInit(): void {
    this.carregarEnderecos()
  }

  private carregarEnderecos(): void {
    this.enderecosService.buscarTodosOsEnderecos()
    .subscribe({
      next: (resposta) => {
        this.enderecos = resposta.results
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }
}
