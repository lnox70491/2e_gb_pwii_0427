import { FuncionariosService } from 'src/app/services/funcionarios/funcionarios.service';
import { Funcionario } from 'src/app/models/Funcionario';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-funcionarios',
  templateUrl: './funcionarios.component.html',
  styleUrls: ['./funcionarios.component.scss']
})
export class FuncionariosComponent implements OnInit {
  funcionarios: Funcionario[]

  constructor(
    private readonly funcionariosService: FuncionariosService
  ) { 
    this.funcionarios = []
  }

  ngOnInit(): void {
    this.carregarFuncionarios()
  }

  private carregarFuncionarios(): void {
    this.funcionariosService.buscarTodosOsFuncionarios()
    .subscribe({
      next: (resposta) => {
        this.funcionarios = resposta.results
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }
}
