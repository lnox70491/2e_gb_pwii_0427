import { EditorasService } from 'src/app/services/editoras/editoras.service';
import { Editora } from 'src/app/models/Editora';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-editoras',
  templateUrl: './editoras.component.html',
  styleUrls: ['./editoras.component.scss']
})
export class EditorasComponent implements OnInit {
  editoras: Editora[]

  constructor(
    private readonly editorasService: EditorasService
  ) {
    this.editoras = []
  }

  ngOnInit(): void {
    this.carregarEditoras()
  }

  private carregarEditoras(): void {
    this.editorasService.buscarTodasAsEditoras()
    .subscribe({
      next: (resposta) => {
        this.editoras = resposta.results
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }
}