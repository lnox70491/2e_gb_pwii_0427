import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditorasRoutingModule } from './editoras-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    EditorasRoutingModule
  ]
})
export class EditorasModule { }
