import { Component, OnInit } from '@angular/core';
import { Autor } from 'src/app/models/Autor';
import { Editora } from 'src/app/models/Editora';
import { AutoresService } from 'src/app/services/autores/autores.service';
import { EditorasService } from 'src/app/services/editoras/editoras.service';
import { LivrosService } from 'src/app/services/livros/livros.service';
import { Pedidos } from 'src/app/models/Pedido';
import { PedidoService } from 'src/app/services/pedidos/pedidos.service';
import { ItensPedidosService } from 'src/app/services/itensPedidos/itensPedidos.service';
import { ItemPedido } from 'src/app/models/ItemPedido';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  autores: Autor[]
  editoras: Editora[]
  pedidos: Pedidos[]
  itensPedidos: ItemPedido []

  constructor(
    private livrosService : LivrosService,
    private autoresService: AutoresService,
    private editorasService: EditorasService,
    private pedidosService: PedidoService,
    private itensPedidosService: ItensPedidosService
  ) { 
      this.autores = []
      this.editoras = []
      this.pedidos = []
      this.itensPedidos = []

    }

  ngOnInit(): void {
        this.autoresService.buscarTodosOsAutores()
        .subscribe({
          next: (resposta: any) => {
            this.autores = resposta.results
            
            console.log(this.autores)
          },
          error: (erro: any) => {
            console.error(erro)
          }
        })
          this.pedidosService.buscarTodosOsPedidos()
          .subscribe({
           next: (resposta: any) => {
             this.pedidos = resposta.results
                      
            console.log(this.pedidos)
            },
           error: (erro: any) => {
           console.error(erro)
            }
           })
           this.itensPedidosService.buscarTodosOsItensPedidos()
           .subscribe({
             next: (resposta: any) => {
               this.itensPedidos = resposta.results
               
               console.log(this.itensPedidos)
             },
             error: (erro: any) => {
               console.error(erro)
             }
           })
        
            
          /*this.livrosService.buscarLivros()
        .subscribe({
         next: (livros) => {
          console.log(livros)
          },

           erro: erro => {
           console.error(erro)
            }
        }}*/
       }
     }
