import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AutoresComponent } from '../admin/autores/autores.component';
import { CadAutorComponent } from './cad-autor.component';

const routes: Routes = [
  {
      path: "",
      component: AutoresComponent
  },
  {
      path: "cad-usuario",
      loadChildren: () => import("../cad-autor/cad-autor.module").then(m => m.CadAutorModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CadAutorRoutingModule { }
