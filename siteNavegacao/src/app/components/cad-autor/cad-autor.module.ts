import { CadAutorComponent } from './cad-autor.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadAutorRoutingModule } from './cad-autor-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CadAutorRoutingModule
  ]
})
export class CadAutorModule { }
