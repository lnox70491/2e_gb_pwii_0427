import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadEnderecoRoutingModule } from './cad-endereco-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CadEnderecoRoutingModule
  ]
})
export class CadEnderecoModule { }
