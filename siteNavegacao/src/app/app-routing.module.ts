import { CadAutorComponent } from './components/cad-autor/cad-autor.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: "",
    pathMatch: "full",
    redirectTo: "home"
  },
  {
    path: "home",
    loadChildren: () => import ("./components/home/home.module").
    then(m => m.HomeModule)
  },
  {
    path: "cadautor",
    loadChildren: () => import ("./components/cad-autor/cad-autor.module").
    then(m => m.CadAutorModule)
  },
  {
    path: "cadeditora",
    loadChildren: () => import ("./components/cad-editora/cad-editora.module").
    then(m => m.CadEditoraModule)
  },
  {
    path: "cadlivro",
    loadChildren: () => import ("./components/cad-livro/cad-livro.module").
    then(m => m.CadLivroModule)
  },
  {
    path: "cadfuncionario",
    loadChildren: () => import ("./components/cad-funcionario/cad-funcionario.module").
    then(m => m.CadFuncionarioModule)
  },
  {
    path: "cadendereco",
    loadChildren: () => import ("./components/cad-endereco/cad-endereco.module").
    then(m => m.CadEnderecoModule)
  },
  {
    path: "admin",
    loadChildren: () => import ("./components/admin/login/login.module").
    then(m => m.LoginModule)
  },
  {
    path: "autores",
    loadChildren: () => import ("./components/admin/autores/autores.module").
    then(m => m.AutoresModule)
  },
  {
    path: "editoras",
    loadChildren: () => import ("./components/admin/editoras/editoras.module").
    then(m => m.EditorasModule)
  },
  {
    path: "funcionarios",
    loadChildren: () => import ("./components/admin/funcionarios/funcionarios.module").
    then(m => m.FuncionariosModule)
  },
  {
    path: "enderecos",
    loadChildren: () => import ("./components/admin/enderecos/enderecos.module").
    then(m => m.EnderecosModule)
  },

  {
    path: "**",
    loadChildren: () => import ("./components/page-not-found/page-not-found.module")
    .then(m => m.PageNotFoundModule)

  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
