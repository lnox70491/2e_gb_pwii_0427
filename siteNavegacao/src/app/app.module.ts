import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CadAutorComponent } from './components/cad-autor/cad-autor.component';
import { CadEditoraComponent } from './components/cad-editora/cad-editora.component';
import { CadLivroComponent } from './components/cad-livro/cad-livro.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { LivrosService } from './services/livros/livros.service';
import { AutoresService } from './services/autores/autores.service';
import { EditorasService } from './services/editoras/editoras.service';
import { ItensPedidosService } from './services/itensPedidos/itensPedidos.service';
import { PedidoService } from './services/pedidos/pedidos.service';
import { EnderecosService } from './services/enderecos/enderecos.service';
import { FuncionariosService } from './services/funcionarios/funcionarios.service';
import { CadEnderecoComponent } from './components/cad-endereco/cad-endereco.component';
import { CadFuncionarioComponent } from './components/cad-funcionario/cad-funcionario.component';
import { LoginComponent } from './components/admin/login/login.component';
import { AutoresComponent } from './components/admin/autores/autores.component';
import { EditorasComponent } from './components/admin/editoras/editoras.component';
import { FuncionariosComponent } from './components/admin/funcionarios/funcionarios.component';
import { EnderecosComponent } from './components/admin/enderecos/enderecos.component';

@NgModule({
  declarations: [
    AppComponent,
    CadAutorComponent,
    CadEditoraComponent,
    CadLivroComponent,
    PageNotFoundComponent,
    CadEnderecoComponent,
    CadFuncionarioComponent,
    LoginComponent,
    AutoresComponent,
    EditorasComponent,
    FuncionariosComponent,
    EnderecosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    LivrosService,
    AutoresService,
    EditorasService,
    ItensPedidosService,
    PedidoService,
    EnderecosService,
    FuncionariosService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
